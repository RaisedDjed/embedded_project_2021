#include "contiki.h"
#include "random.h"
#include "net/ipv6/simple-udp.h"
#include "sys/log.h"
#include "messageformat.c"

#define LOG_MODULE "Temperature"
#define LOG_LEVEL LOG_LEVEL_INFO

#define WITH_SERVER_REPLY  1
#define UDP_CLIENT_PORT    8765
#define UDP_SERVER_PORT    3000

#define SEND_INTERVAL          (10 * CLOCK_SECOND)

static struct simple_udp_connection udp_conn;

/*---------------------------------------------------------------------------*/
PROCESS(udp_client_process, "UDP client");
AUTOSTART_PROCESSES(&udp_client_process);

/*---------------------------------------------------------------------------*/
static void
udp_rx_callback(struct simple_udp_connection *c,
                const uip_ipaddr_t *sender_addr,
                uint16_t sender_port,
                const uip_ipaddr_t *receiver_addr,
                uint16_t receiver_port,
                const uint8_t *data,
                uint16_t datalen) {

    LOG_INFO("Received response '%.*s' from ", datalen, (char *) data);
    LOG_INFO_6ADDR(sender_addr);
#if LLSEC802154_CONF_ENABLED
    LOG_INFO_(" LLSEC LV:%d", uipbuf_get_attr(UIPBUF_ATTR_LLSEC_LEVEL));
#endif
    LOG_INFO_("\n");

}

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(udp_client_process, ev, data) {
    static struct etimer periodic_timer;

    uip_ipaddr_t dest_ipaddr;

    PROCESS_BEGIN();

    /* Initialize UDP connection */
    simple_udp_register(&udp_conn, UDP_CLIENT_PORT, NULL,
    UDP_SERVER_PORT, udp_rx_callback);

    etimer_set(&periodic_timer, SEND_INTERVAL);

    while(1) {
        PROCESS_WAIT_EVENT_UNTIL (etimer_expired(&periodic_timer));

        uip_ip6addr(&dest_ipaddr,0xbbbb,0,0,0,0,0,0,0x1);

        char min = -50, max = 50;
        char temperature = (random_rand() % (max - min + 1)) + min; /* to select a random temperature*/
        LOG_INFO("temperature %d \n", temperature);

        LOG_INFO("Sending response to ");
        LOG_INFO_6ADDR(&dest_ipaddr);
        LOG_INFO_("\n");

        /* Building the message to send */
        struct Message m;
        m.version=0;
        m.nonce=1;
        m.type_device=2;
        m.type_message=3; // It is not used here so we just set a number
        m.payload=temperature;
        char str[8];
        to_string(str, m);
        simple_udp_sendto(&udp_conn, str, 8, &dest_ipaddr);

        etimer_set(&periodic_timer, SEND_INTERVAL);
    }

    PROCESS_END();

}
/*---------------------------------------------------------------------------*/
