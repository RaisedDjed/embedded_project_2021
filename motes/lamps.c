#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"
#include "dev/leds.h"
#include "sys/log.h"
#include "messageformat.c"

#define MAX_PAYLOAD_LEN 8

#define LOG_MODULE "Light"
#define LOG_LEVEL LOG_LEVEL_INFO

static struct uip_udp_conn *server_conn;
static char buf[MAX_PAYLOAD_LEN];
static uint16_t len;


/*---------------------------------------------------------------------------*/

PROCESS(udp_server_process, "UDP server process");
AUTOSTART_PROCESSES(&udp_server_process);


static void tcpip_handler(void) {

    memset(buf, 0, MAX_PAYLOAD_LEN); // prepare the buf variable by setting all the allocated memory to 0
    if(uip_newdata()) { // the remote host of the connection has sent new data

        len = uip_datalen(); // get the size of the incoming data
        memcpy(buf, uip_appdata, len); // copy the incoming data into buf variable

        struct Message message = to_struct(buf);
        LOG_INFO("%u bytes from [", len);
        LOG_INFO_6ADDR(&UIP_IP_BUF->srcipaddr); // print the ipv6 address of the remote host
        LOG_INFO_("]:%u\n", UIP_HTONS(UIP_UDP_BUF->srcport)); // print its port number

        LOG_INFO("Payload get: %u\n", (unsigned int)message.payload);
        if (message.payload == 1) {
            leds_on(LEDS_RED);
            LOG_INFO("The light has been switched on\n");
        }
        else if (message.payload == 0) {
            leds_off(LEDS_RED);
            LOG_INFO("The light has been switched off\n");
        }
        else if (message.payload == 2) {
            leds_toggle(LEDS_RED);
            LOG_INFO("The light has been switched\n");
        }

    }
    return;
}

PROCESS_THREAD(udp_server_process, ev, data) {

    PROCESS_BEGIN();
    LOG_INFO("Starting the server\n");

    server_conn = udp_new(NULL, UIP_HTONS(0), NULL);
    udp_bind(server_conn, UIP_HTONS(3000));

    LOG_INFO("Listen port: 3000, TTL=%u\n", server_conn->ttl);

    while(1) {
        PROCESS_YIELD();
        if(ev == tcpip_event) {
            LOG_INFO("Network event triggered\n");
            tcpip_handler();
        }
    }

    PROCESS_END();
}
