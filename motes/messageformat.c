#include <stdio.h>
#include <stdint.h>


struct Message{
    uint8_t version;
    uint8_t nonce;
    uint8_t type_device;
    uint8_t type_message;
    uint32_t payload;
};

void to_string(char* str, struct Message m){
    //Get a 8 byte char buffer as input.
    str[0]=m.version;
    str[1]=m.nonce;
    str[2]=m.type_device;
    str[3]=m.type_message;
    str[4]=m.payload>>24;
    str[5]=m.payload>>16;
    str[6]=m.payload>>8;
    str[7]=m.payload;
}

struct Message to_struct(char* str){
    //Get a 8 byte char buffer as input.
    struct Message m;
    m.version=str[0];
    m.nonce=str[1];
    m.type_device=str[2];
    m.type_message=str[3];
    uint8_t* v=(uint8_t*)&m.payload;
    v[0]=str[7];
    v[1]=str[6];
    v[2]=str[5];
    v[3]=str[4];
    return m;
}

void test(){
    struct Message m;
    char str[8];
    m.version=0;
    m.nonce=1;
    m.type_device=2;
    m.type_message=3;
    m.payload=4514587;
    to_string(str, m);
    struct Message res=to_struct(str);
    if((res.version!=m.version)||(res.nonce!=m.nonce)||(res.type_device!=m.type_device)||(res.type_message!=m.type_message)||(res.payload!=m.payload)){
        printf("fail");
    }
}

