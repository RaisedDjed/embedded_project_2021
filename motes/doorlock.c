#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"
#include "dev/leds.h"
#include "sys/log.h"
#include "messageformat.c"

#define MAX_PAYLOAD_LEN 8

#define LOG_MODULE "Door"
#define LOG_LEVEL LOG_LEVEL_INFO

static struct uip_udp_conn *server_conn;
static char buf[MAX_PAYLOAD_LEN];
static uint16_t len;
static uint32_t state;


/*---------------------------------------------------------------------------*/

PROCESS(udp_server_process, "UDP server process");
AUTOSTART_PROCESSES(&udp_server_process);


static void tcpip_handler(void) {

    memset(buf, 0, MAX_PAYLOAD_LEN); // prepare the buf variable by setting all the allocated memory to 0
    if(uip_newdata()) { // the remote host of the connection has sent new data
        
        len = uip_datalen(); // get the size of the incoming data
        memcpy(buf, uip_appdata, len); // copy the incoming data into buf variable

        struct Message message = to_struct(buf);
        LOG_INFO("%u bytes from [", len);
        LOG_INFO_6ADDR(&UIP_IP_BUF->srcipaddr); // print the ipv6 address of the remote host
        LOG_INFO_("]:%u\n", UIP_HTONS(UIP_UDP_BUF->srcport)); // print its port number

        LOG_INFO("Payload get: %u\n", (unsigned int)message.payload);
        if(message.payload == 1){
            LOG_INFO("The door is open\n");
            message.type_message = 1; // Will be used on the server to answer back
            state = 1;
        }else if(message.payload == 0){
            LOG_INFO("The door is closed\n");
            message.type_message = 1;
            state = 0;
        } else if(message.payload == 2){
            if(state == 1){
                LOG_INFO("The door is closed\n");
                message.type_message = 1;
                state = 0;
            } else if(state == 0){
                LOG_INFO("The door is opened\n");
                message.type_message = 1;
                state = 1;
            }
        }

        message.payload = state;
        LOG_INFO("Payload to return: %u\n", (unsigned int)message.payload);

        char str[8];
        to_string(str, message);

        uip_ipaddr_copy(&server_conn->ripaddr, &UIP_IP_BUF->srcipaddr); // set incoming data source address as the destination for the server to reach
        server_conn->rport = UIP_HTONS(3000); // sending port
        uip_udp_packet_send(server_conn, str, 8); // send the same read data to the server
        
        /* Restore server connection to allow data from any node */
        uip_create_unspecified(&server_conn->ripaddr); // delete the address as destination for the server
        server_conn->rport = 0; // set the destination port to 0
    }
    return;
}

PROCESS_THREAD(udp_server_process, ev, data) {

    PROCESS_BEGIN();
    LOG_INFO("Starting the server\n");

    server_conn = udp_new(NULL, UIP_HTONS(0), NULL);
    udp_bind(server_conn, UIP_HTONS(3000));

    LOG_INFO("Listen port: 3000, TTL=%u\n", server_conn->ttl);

    while(1) {
        PROCESS_YIELD();
        if(ev == tcpip_event) {
            LOG_INFO("Network event triggered\n");
            tcpip_handler();
        }
    }

    PROCESS_END();
}
