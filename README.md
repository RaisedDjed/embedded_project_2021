# embedded_project_2021

Embedded project UCL 2021

## Motes side
- Load the border router of the rpl in cooja
- Load each devices in cooja

After loading the motes on cooja, we run the tunslip6:
	sudo ./tunslip6  -a "ip" -p "port" bbbb::1/64

Next launch the server
### Server side
To run the python server, there are 2 modifications to make in the code to determine the listening address of the server.
The line to change is number 515 , where you can find this line : 

server_thread = threading.Thread(None, Server, "server", ("bbbb::1", 3000))

The last parameter is with format ipv6. This address is the one from the border gateway , through the tool tunslip6.
We usually used, with our tunslip6 configured, the IPv6 "bbbb::1" and we listen on port 3000

    launch the server by typing: python3 server.py
