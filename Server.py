import socketserver
import threading
import socket
import secrets
import time
import os
import json
from datetime import date


# -- Variable declaration (Global to allow Thread interaction)

global data
data = {}

global automation
automation = {}

global door_lock_ack
door_lock_ack = {}

running_lock = threading.Lock()
writing_lock = threading.Lock()

# -- Save Management

def load_data():
    try:
        f = open("save.jopage")
        f2 = open("save_auto.jopage")
        content = f.read()
        content2 = f2.read()
        global data
        global automation
        data = json.loads(content)
        automation = json.loads(content2)
    except FileNotFoundError:
        print("No save found, will be created when the server will close")

def save_data():
    f = open("save.jopage", "w")
    f.write(json.dumps(data))
    f2 = open("save_auto.jopage", "w")
    f2.write(json.dumps(automation))

# -- Message Format Parser

def from_bytes_to_dict(msg):
    new_data = {}
    new_data["version"] = msg[0]
    new_data["nonce"] = msg[1]
    new_data["type_device"] = msg[2]
    new_data["type_message"] = msg[3]
    new_data["data_message"] = int.from_bytes(msg[4:], byteorder="big", signed=True)
    return new_data

def from_dict_to_bytes(msg):

    bytes_list = []
    bytes_list.append(msg["version"])
    bytes_list.append(msg["nonce"])
    bytes_list.append(msg["type_device"])
    bytes_list.append(msg["type_message"])
    
    hexvalue = hex(msg["data_message"])[2:]
    hexvalue = (8 - len(hexvalue)) * '0' + hexvalue
    data_message = msg["data_message"].to_bytes( 4, byteorder="big", signed=True)
    for byt in data_message:
        bytes_list.append(byt)
    return bytes(bytes_list)

def create_new_message():
    return {"version": 1, "nonce": secrets.randbits(8), "type_device": 0, "type_message": 0, "data_message": 0}

# -- UDP Sending Communication

def send_datagram(address, packet_data):
    device_socket = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
    tuple_addr = (address, 3000, 0, 0)
    data[address] = packet_data
    device_socket.sendto(from_dict_to_bytes(packet_data), tuple_addr)
    
    if packet_data["type_device"] == 3:
        door_lock_ack[address] = 0

# -- UDP6Server -> Override socketserver.UDPServer to manage IPv6
class UDP6Server(socketserver.UDPServer):
    address_family = socket.AF_INET6

class Server:

    """
    Main Network instance
    Create the thread and start the server
    """

    def __init__(self, address, port):
        self.create_network_listener(address, port)

    def create_network_listener(self, address, port):
        self.server_instance = UDP6Server((address, port), ServerTask)

        server_running_thread = threading.Thread(None, self.server_instance.serve_forever)
        server_running_thread.start()
    
        running_lock.acquire()
        self.server_instance.shutdown()
        self.server_instance.server_close()
        running_lock.release()
        
class ServerTask(socketserver.BaseRequestHandler):
    
    """
    UDP IPv6 Message Handler
    Perform the transformation of the received Packets, and check/apply the automation set up
    """

    def handle(self):
        #device_id = str(self.client_address[0])+";"+str(self.client_address[1])+";"+str(self.client_address[2])+";"+str(self.client_address[3])
        device_id = str(self.client_address[0])

        tmp_data = from_bytes_to_dict(self.request[0])

        if(tmp_data["type_message"] == 1 and tmp_data["nonce"] == data[device_id]["nonce"]): #Reponse
            door_lock_ack[device_id] = 1

        data[device_id] = tmp_data
        data[device_id]["last_update"] = time.time();


        self.check_automation(device_id)

        # Need to save data ? 
        # TODO : Check automation

    def check_automation(self, device_id):
        for key in automation.keys():
            orig_ip = automation[key][0]

            if orig_ip == device_id:
                # Apply the reaction
                trigger = automation[key][1]
                dest_ip = automation[key][2]
                reaction = automation[key][3]
                self.apply_automation(orig_ip, trigger, dest_ip, reaction)
            
    def apply_automation(self, orig_ip, trigger, dest_ip, reaction):
        device_type = data[orig_ip]["type_device"]

        new_packet_data = create_new_message()
        new_packet_data["type_device"] = data[dest_ip]["type_device"]
        new_packet_data["type_message"] = 0

        if device_type == 1: # Proximity Sensor
            if trigger[0] == 11: # Proximity detected
                new_packet_data["data_message"] = reaction[0]%10
                send_datagram(dest_ip, new_packet_data)
        if device_type == 2:
            value = data[dest_ip]["data_message"]
            if trigger[0] == 21: # Thermometer temp above ...
                if value >= trigger[1]:
                    new_packet_data["data_message"] = reaction[0]%10
                    send_datagram(dest_ip, new_packet_data)
            if trigger[0] == 22: # Thermometer temp between ...
                if value >= trigger[1] and value <= trigger[2]:
                    new_packet_data["data_message"] = reaction[0]%10
                    send_datagram(dest_ip, new_packet_data)
            if trigger[0] == 23: # Thermometer temp under ...
                if value <= trigger[1]:
                    new_packet_data["data_message"] = reaction[0]%10
                    send_datagram(dest_ip, new_packet_data)


class ServerInterface:

    """
    Command-line interface for the server
    """

    automation_index = 1

    def __init__(self):

        running_lock.acquire()
        res = -1

        print("\n\n=== Welcome on the JOPAGE server ===\n")
        while res != '0':
            #os.system("clear||cls")
            
            print("\n====================================\n")
            print("\t1. Manage Devices")
            print("\t2. Manage Automation")
            print("\t0. shutdown the server\n")

            res = input("Choice : ")
            print("\n====================================\n")

            if(res == '1'):
                self.manage_devices()
                    
            elif(res == '2'):
                self.manage_automation()                

        running_lock.release()
        save_data()

    def manage_devices(self):
        print("\n====================================\n")
        print("\t1. Add a device ")
        print("\t2. Remove a device")
        print("\t3. Manage actions with devices\n")
        inp = input("Choice : ")
        print("\n====================================\n")
        if inp == '1':
            self.add_device()
        if inp == '2':
            self.remove_device()
        if inp == '3':
            self.print_actions()

    def add_device(self):
        print("\n====================================\n")
        device_address = input("Enter the ipv6 address of the devices : ")
        print("\nHere are the devices types available, choose one of these :")
        print("0 => Lamp")
        print("1 => Proximity Sensor")
        print("2 => Temperature Sensor")
        print("3 => Door Lock\n")
        type_device = input("Device Type Choice : ")
        print("\n====================================\n")
        if(type_device in ['0', '1', '2', '3']):
            data[device_address] = {"version": 1, "nonce": secrets.randbits(8), "type_device": int(type_device), "type_message": 0, "data_message": 0, "last_update": time.time()}
            print("Device Added !")
        else:
            print("An error occured")

    def remove_device(self):
        print("\n====================================\n")
        index_arr = self.list_devices()
        remove_id = input("Select the device you want to remove (0 to cancel) : ")
        print("\n====================================\n")
        if(remove_id in index_arr.keys()):
            index_arr.pop(int(remove_id))
            print("Device removed")

    def list_devices(self, listening_only=False, sending_only=False):

        """
        List the devices depending on the condition, if we want only to check the listening or sending devices, for automation purposes as an example
        """

        print("===== ===== ===== =====")
        device_type_trans = {0: "Lamp", 1: "Proximity Sensor", 2: "Temperature Sensor", 3: "Door Lock"}
        index_assign = {}
        i = 1
        for key in data.keys():
            if sending_only == False and listening_only == False:
                print(i," => ", end="\t")
                print("device address :", key, "\t\ttype :", device_type_trans[data[key]["type_device"]])
                index_assign[i] = key
                i += 1
            if (sending_only and data[key]["type_device"] in [1, 2]):
                print(i," => ", end="\t")
                print("device address :", key, "\t\ttype :", device_type_trans[data[key]["type_device"]])
                index_assign[i] = key
                i += 1
            if (listening_only and data[key]["type_device"] in [0, 3]):
                print(i," => ", end="\t")
                print("device address :", key, "\t\ttype :", device_type_trans[data[key]["type_device"]])
                index_assign[i] = key
                i += 1
        print("===== ===== ===== =====")
        return index_assign

    def print_information(self, device_id, type):

        """
        Print the information returned by the devices and ask to perform actions on the devices accepting actions
        """

        print("Device ID :", device_id, end="\t\t")
        if type == 2:
            # Read only sensor
            data_time = time.gmtime(data[device_id]["last_update"])
            print("Temperature :",data[device_id]["data_message"], "°C\t", "updated :", time.strftime("%a, %d %b %Y %H:%M:%S +0000", data_time))

        elif type == 3:
            # Read Write sensor
            if device_id not in door_lock_ack:
                door_lock_ack[device_id] = 1
            print("Lock status:", data[device_id]["data_message"], "\t Received and verified ? :", door_lock_ack[device_id])
            req = input("What actions you want to achieve ?\n1. Lock the door \n2. Unlock the door\n3. Switch the state\nChoice :")
            new_message = create_new_message()
            new_message["type_device"] = 3
            new_message["type_message"] = 0
            if(req == '1'):
                door_lock_ack[device_id] = 0                
                new_message["data_message"] = 0
                data[device_id]["data_message"] = 0
                send_datagram(device_id, new_message);
            elif(req == '2'):
                door_lock_ack[device_id] = 0
                new_message["data_message"] = 1
                data[device_id]["data_message"] = 1
                send_datagram(device_id, new_message);
            elif(req == '3'):
                door_lock_ack[device_id] = 0
                new_message["data_message"] = 2
                data[device_id]["data_message"] = 2
                send_datagram(device_id, new_message);
            else:
                print("nothing (or wrong input) provided")

        elif type == 0:
            # Write sensor
            print("Light status:", data[device_id]["data_message"])
            req = input("What actions you want to achieve ?\n1. Switch off the lamp\n2. Light the lamp\n3. Switch the state\nChoice :")
            new_message = create_new_message()
            new_message["type_device"] = 0
            new_message["type_message"] = 0
            if(req == '1'):
                new_message["data_message"] = 0
                data[device_id]["data_message"] = 0
                send_datagram(device_id, new_message);
            elif(req == '2'):
                new_message["data_message"] = 1
                data[device_id]["data_message"] = 1
                send_datagram(device_id, new_message);
            elif(req == '3'):
                new_message["data_message"] = 2

                if(data[device_id]["data_message"] == 0):
                    data[device_id]["data_message"] = 1
                else:
                    data[device_id]["data_message"] = 0
                
                send_datagram(device_id, new_message);
            else:
                print("nothing (or wrong input) provided")

        elif type == 1:
            # Read only sensor
            data_time = time.gmtime(data[device_id]["last_update"])
            print("Last Proximity Detected :", time.strftime("%a, %d %b %Y %H:%M:%S +0000", data_time))

    def print_actions(self):
        print("\n====================================\n")
        index_assign = self.list_devices()
        choose = int(input("Select the device you want to check (0 to cancel) :"))
        print("\n====================================\n")
        if(choose in index_assign):
            self.print_information(index_assign[choose], data[index_assign[choose]]["type_device"])

    def manage_automation(self):
        os.system("clear||cls")
        print("\n====================================\n")
        _ = self.list_automation()
        print("\t1. Add an automation")
        print("\t2. Remove an automation")

        inp = input("Choice : ")
        print("\n====================================\n")
        if (inp in ['1', '2']):
            if inp == '1':
                self.create_automation()
            else:
                self.remove_automation()
    
    
    def create_automation(self):

        """
        Create the automation by choosing the trigger, and its event condition. After this, we choose the reacting device, with the action linked to the automation
        """

        print("\n====================================\n")
        print("Choose a device to be the trigger :")
        index_assign = self.list_devices(sending_only=True)

        inp = input("Choice :")
        print("\n====================================\n")
        if(inp == ""):
            inp = -1
        inp = int(inp)
        if inp in index_assign.keys():
            trigger = self.setup_trigger_event(index_assign[inp])
            if trigger != False:
                print("\n====================================\n")
                print("Choose the device for the reaction ...")
                reaction_assign = self.list_devices(listening_only=True)
                react_inp = input("Choice :")
                print("\n====================================\n")
                if react_inp == "":
                    react_inp = -1
                react_inp = int(react_inp)
                if react_inp in reaction_assign.keys():
                    reaction = self.setup_reaction_event(reaction_assign[react_inp])

                    if reaction != False:
                        automation_tuple = (index_assign[inp], trigger, reaction_assign[react_inp], reaction)
                        automation[self.automation_index] = automation_tuple
                        self.automation_index += 1
                    else:
                        print("An error occured ...")
                else:
                    print("An error occured ...");
        else:
            print("An error occured ...")

    def remove_automation(self):
        index_auto = self.list_automation()
        inp = int(input("Choice : "))
        automation.pop(index_auto[inp])

    def list_automation(self):
        automation_index = {}
        i = 1
        for key in automation.keys():
            value = automation[key]
            print(i,"\t", "Trigger Node Address :", value[0], "\tReaction Node Address :", value[2])
            automation_index[i] = key
            i += 1
        return automation_index

    def setup_trigger_event(self, device_id):
        type = data[device_id]["type_device"]
        print("Choose the event (0 to cancel) :")
        if(type == 0): # Lamp
            print("1. Lamp Light set to On")                #id 1
            print("2. Lamp Light set to Off")               #id 2
            print("3. Lamp Light changed of state")         #id 3
            inp = input("Choice :")
            if(inp == '1'):
                return (1,)
            if(inp == '2'):
                return (2,)
            if(inp == '3'):
                return (3,)

        if(type == 1): # Proximity sensor
            print("1. Proximity detected")                     #id 11
            inp = input("Choice :")
            if(inp == '1'):
                return (11,)

        if(type == 2): # Temperature
            
            print("1. Temperature above ...")               #id 21
            print("2. Temperature between ...")             #id 22
            print("3. Temperature under ...")               #id 23
            inp = input("Choice :")
            if(inp == '1'):
                temp = int(input("Temperature (in degrees) :"))
                return (21, temp)
            if(inp == '2'):
                temp_min = int(input("Temperature min (in degrees) :"))
                temp_max = int(input("Temperature min (in degrees) :"))
                return (22, temp_min, temp_max)
            if(inp == '3'):
                temp = int(input("Temperature (in degrees) :"))
                return (23, temp)

        if(type == 3): # Door Lock
            print("1. Door Locked")                         #id 31
            print("2. Door Unlocked")                       #id 32
            print("3. Door Changed of State")               #id 33
            inp = input("Choice :")
            if(inp == '1'):
                return (31,)
            if(inp == '2'):
                return (32,)
            if(inp == '3'):
                return (33,)

        return False
            

    def setup_reaction_event(self, device_id):
        type = data[device_id]["type_device"]
        print("Choose the reaction event (0 to cancel) :")

        if type == 0:
            print("1. Lamp Light On")                   # event 1
            print("2. Lamp Light Off")                  # event 2
            print("3. Lamp Light changed of state")     # event 3
            inp = input("Choice :")
            if inp == '1':
                return (1,)
            if inp == '2':
                return (2,)
            if inp == '3':
                return (3,)

        if type == 3:
            print("1. Lock the door")                   # event 31
            print("2. Unlock the door")                 # event 32
            print("3. Door Lock Changed of State")      # event 33
            inp = input("Choice :")
            if inp == '1':
                return (31,)
            if inp == '2':
                return (32,)
            if inp == '3':
                return (33,)

        return False


load_data()

interface_thread = threading.Thread(None, ServerInterface, "interface", ())
interface_thread.start()

server_thread = threading.Thread(None, Server, "server", ("::1", 5600))
server_thread.start()